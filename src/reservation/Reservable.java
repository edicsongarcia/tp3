package reservation;

import transport.EndroitDisponible;
import transport.EtatEndroit;

public class Reservable {

  EtatEndroit etat;

  public Reservable() {
    this.etat = new EndroitDisponible(this);
  }

  public void changeStateReserved () {
    this.etat.reserver();
  }

  public void changeStateAvailable () {
    this.etat.annuler();
  }

  public void changeStateConfirmed () {
    this.etat.confirmer();
  }

  public void changerEtat (EtatEndroit etat) {
    this.etat = etat;
  }

  public EtatEndroit getEtat() {
    return etat;
  }
}
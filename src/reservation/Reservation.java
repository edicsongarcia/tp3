package reservation;

public  abstract class Reservation {


  private String name;
  private String family;
  private String passport_number;
  private String IdTransport;
  private String Credit_cart;
  private String Date_expire;
  private String reservation;
  private String class_passanger;
  private String SeatNumber;


  public Reservation(String name, String family, String passport_number, String idTransport, String credit_cart, String date_expire, String reservation, String class_passanger, String seatNumber) {
    this.name = name;
    this.family = family;
    this.passport_number = passport_number;
    IdTransport = idTransport;
    Credit_cart = credit_cart;
    Date_expire = date_expire;
    this.reservation = reservation;
    this.class_passanger = class_passanger;
    SeatNumber = seatNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFamily() {
    return family;
  }

  public void setFamily(String family) {
    this.family = family;
  }

  public String getPassport_number() {
    return passport_number;
  }

  public void setPassport_number(String passport_number) {
    this.passport_number = passport_number;
  }

  public String getIdTransport() {
    return IdTransport;
  }

  public void setIdTransport(String idTransport) {
    IdTransport = idTransport;
  }

  public String getCredit_cart() {
    return Credit_cart;
  }

  public void setCredit_cart(String credit_cart) {
    Credit_cart = credit_cart;
  }

  public String getDate_expire() {
    return Date_expire;
  }

  public void setDate_expire(String date_expire) {
    Date_expire = date_expire;
  }

  public String getReservation() {
    return reservation;
  }

  public void setReservation(String reservation) {
    this.reservation = reservation;
  }

  public String getClass_passanger() {
    return class_passanger;
  }

  public void setClass_passanger(String class_passanger) {
    this.class_passanger = class_passanger;
  }

  public String getSeatNumber() {
    return SeatNumber;
  }

  public void setSeatNumber(String seatNumber) {
    SeatNumber = seatNumber;
  }

  @Override
  public String toString() {
    return "Reservation{" +
            "name='" + name + '\'' +
            ", family='" + family + '\'' +
            ", passport_number='" + passport_number + '\'' +
            ", IdTransport='" + IdTransport + '\'' +
            ", Credit_cart='" + Credit_cart + '\'' +
            ", Date_expire='" + Date_expire + '\'' +
            ", reservation='" + reservation + '\'' +
            ", class_passanger='" + class_passanger + '\'' +
            ", SeatNumber='" + SeatNumber + '\'' +
            '}';
  }
}

package gestion;

import java.util.*;

import Location.*;


public class GestionLocation implements Gestion {

  private ArrayList <Location> locations;

  public GestionLocation() {

    locations = new ArrayList<Location>();
  }

  public GestionLocation(AbstractFabrique fabrique, String id,String vil) {
    locations = new ArrayList<>();
  }

  public void creer(AbstractFabrique fabrique, String id, String ville) {
    locations.add( fabrique.creerLocation(id, ville) );
  }

  public void creer() {

  }

  public void modifier(int id) {

  }

  public void supprimer(int id) {

  }

  public ArrayList<Location> getLocations() {
    return locations;
  }

}
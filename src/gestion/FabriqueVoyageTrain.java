package gestion;

import Location.*;
import transport.*;
import voyage.*;
import compagnie.*;

import java.util.ArrayList;

public class FabriqueVoyageTrain extends AbstractFabrique {

  /**
   *
   * @param id
   * @param vil
   */
  public Location creerLocation(String id,String vil) {
    return new Gares(id,vil);

  }

  @Override
  public Trajet creerTrajet(String company, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    return new Ligne(company, id, villeDepart, villeArrive, dateDepart, dateArrive, heureDepart, heureArrive);
  }

  public Trajet creerTrajet(String id) {
    return new Ligne(id);

  }

  public Compagnie creerCompagnie(String id, int prixSection) {
    return new Terrestre(id, prixSection);

  }

  @Override
  public MoyenTransport creerMoyenDeTransport(String id, ArrayList<Section> sections) {
    return new Train(id, sections);
  }

  public MoyenTransport creerMoyenDeTransport(String id, int ranges) {
    return new Train(id, ranges);
  }

  @Override
  public Section creerSection(String id, int nbSeats, String disposition, int prixSection) {
    Double price;
    int priceParSection;
    if (id.equalsIgnoreCase("1")) {
      price = prixSection * 0.6;
      priceParSection = price.intValue();
      return new Premier("P", nbSeats, disposition, priceParSection );
    } else {
      price = prixSection * 0.5;
      priceParSection = price.intValue();
      return new Economique("E", nbSeats, disposition, prixSection);
    }
  }
}
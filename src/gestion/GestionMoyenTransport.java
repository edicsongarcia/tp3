package gestion;

import transport.MoyenTransport;
import transport.Section;

import java.util.ArrayList;


public class GestionMoyenTransport implements Gestion {

  private ArrayList<MoyenTransport> transports;


  public GestionMoyenTransport() {
    transports = new ArrayList<>();
  }

  public void creer (AbstractFabrique fabrique, String id, ArrayList<Section> sections) {
    transports.add( fabrique.creerMoyenDeTransport(id, sections) );
  }

  public void creer (AbstractFabrique fabrique, String id, int ranges) {
    transports.add( fabrique.creerMoyenDeTransport(id, ranges) );
  }

  public ArrayList<MoyenTransport> getTransports() {
    return transports;
  }

  @Override
  public void creer() {

  }

  @Override
  public void modifier(int id) {

  }

  @Override
  public void supprimer(int id) {

  }
}

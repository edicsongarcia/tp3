package gestion;

import Location.*;
import compagnie.*;
import transport.*;
import voyage.*;

import java.util.ArrayList;


public class FabriqueVoyageBateau extends AbstractFabrique {

  /**
   *
   * @param id
   * @param vil
   */
  public Location creerLocation(String id,String vil) {
    return new Port(id,vil);

  }

  @Override
  public Trajet creerTrajet(String company, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    return new Croisier(company, id, villeDepart, villeArrive, dateDepart, dateArrive, heureDepart, heureArrive);
  }

  public Trajet creerTrajet(String id) {
    return new Croisier(id);

  }

  @Override
  public MoyenTransport creerMoyenDeTransport(String id, ArrayList<Section> sections) {
    return new Bateau(id, sections);
  }

  public Compagnie creerCompagnie(String id, int prixSection) {
    return new Maritime(id, prixSection);

  }

  public MoyenTransport creerMoyenDeTransport(String id, int nbCabines) {
    return new Bateau(id, nbCabines);
  }

  @Override
  public Section creerSection(String id, int nbSeats, String disposition, int prixSection) {
    Double price;
    int priceParSection;
    if (id.equalsIgnoreCase("1")) {
      price = prixSection * 0.5;
      priceParSection = price.intValue();
      return new Interieur("I", nbSeats, priceParSection);
    } else if (id.equalsIgnoreCase("2")) {
      price = prixSection * 0.75;
      priceParSection = price.intValue();
      return new Ocean("O", nbSeats, priceParSection);
    } else if (id.equalsIgnoreCase("3")) {
      price = prixSection * 0.9;
      priceParSection = price.intValue();
      return new Suite("S", nbSeats, priceParSection);
    } else if (id.equalsIgnoreCase("4")) {
      price = prixSection * 0.9;
      priceParSection = price.intValue();
      return new Famille("F", nbSeats, priceParSection);
    } else {
      return new FamilleDeLuxe("D", nbSeats, prixSection);
    }
  }
}
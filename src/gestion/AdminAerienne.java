package gestion;

import Location.Location;
import compagnie.Compagnie;
import transport.MoyenTransport;
import transport.Section;
import voyage.Trajet;

import java.util.ArrayList;

public class AdminAerienne extends Administrateur {
  AbstractFabrique fabriqueAerienne = new FabriqueVoyageAvion();
  GestionLocation gestionLocation = null;
  GestionCompagnie gestionCompagnie = null;
  GestionTrajet gestionTrajet = null;
  GestionMoyenTransport gestionMoyenTransport = null;

  public AdminAerienne () {
    this.gestionLocation = new GestionLocation();
    this.gestionCompagnie = new GestionCompagnie();
    this.gestionTrajet = new GestionTrajet();
    this.gestionMoyenTransport = new GestionMoyenTransport();

    //// Hardcoded database
    creerLocation("QBC", "Quebec");
    creerLocation("MTL", "Montreal");
    creerLocation("TOR", "Toronto");
    creerLocation("OTW", "Ottawa");
    creerCompagnie("AirCanada".substring(0, 6), 500);
    creerCompagnie("Transat".substring(0, 6), 30);
    creerCompagnie("ViaRail".substring(0, 6), 400);
    creerCompagnie("Concordia".substring(0, 6), 250);
    creerTrajet("AirCanada".substring(0, 6), "Toronto", "Montreal",
            "25-mars", "25-Mars", "09:00",
            "11:00");
    creerTrajet("Transat".substring(0, 6), "Ottawa", "Toronto",
            "19-Mars", "19-Mars", "13:30",
            "14:10");
    creerTrajet("ViaRail".substring(0, 6), "Quebec", "Toronto",
            "29-Dic", "31-Dic", "19:00",
            "06:00");
    creerTrajet("Concorde".substring(0, 6), "Montreal", "Montreal",
            "21-May", "28-May", "06:00",
            "22:00");
  }


  public void creerLocation (String id, String ville) {
    gestionLocation.creer(fabriqueAerienne, id, ville);
  }

  public void creerCompagnie (String nomComapgnie, int prixSection) {
    String idCompany = nomComapgnie.substring(0, 6);
    if (searchCompagnie(idCompany)) {
      idCompany = nomComapgnie.substring(0, 4) + gestionCompagnie.getCompagnies().size() + "";
      if (idCompany.length() <= 6) {
        idCompany += "1";
      }
    }
    gestionCompagnie.creer(fabriqueAerienne, idCompany, prixSection);
  }

  public boolean searchCompagnie (String compagnie) {
    String company = compagnie.substring(0,6);
    for (int i = 0; i < gestionCompagnie.getCompagnies().size() ; i++) {
      if ( company.equals(gestionCompagnie.getCompagnies().get(i).getId()) ) {
        return true;
      }
    }
    return false;
  }

  public int getPriceSectionCompagnieById (String compagnie) {
    String company = compagnie.substring(0,6);
    for (int i = 0; i < gestionCompagnie.getCompagnies().size() ; i++) {
      if ( company.equals(gestionCompagnie.getCompagnies().get(i).getId()) ) {
        return gestionCompagnie.getCompagnies().get(i).getPrixSection();
      }
    }
    return 0;
  }

  public boolean searchLocation (String ville) {
    for (int i = 0; i < gestionLocation.getLocations().size() ; i++) {
      if ( ville.equals(gestionLocation.getLocations().get(i).getVille()) ) {
        return true;
      }
    }
    return false;
  }

  public String getIdCompagnie (String company) {
    String compagnie = company.substring(0,6);
    for (int i = 0; i < gestionCompagnie.getCompagnies().size() ; i++) {
      if ( compagnie.equals(gestionCompagnie.getCompagnies().get(i).getId()) ) {
        return gestionCompagnie.getCompagnies().get(i).getId();
      }
    }
    return null;
  }

  // Creation ID Unique
  public String creerID (String company, String dateDebut, String dateArrive, String heureDebut, String heureArrive) {
    return company.substring(0,2) + dateDebut.substring(0,1) + dateArrive.substring(0,1) + heureDebut.substring(0,1) + heureArrive.substring(0,1);
  }

  public void creerTrajet(String company, String locationDepart, String locationArrive, String  dateDepart, String dateArrive, String heureDepart, String heureArrive ) {
    String id = creerID(company, dateDepart, dateArrive, heureDepart, heureArrive);
    gestionTrajet.creerVol(fabriqueAerienne, company, id, locationDepart, locationArrive,  dateDepart, dateArrive, heureDepart, heureArrive );
  }

  public void creerTransport (String id, int rangees) {
    gestionMoyenTransport.creer(fabriqueAerienne, id, rangees);
  }

  public MoyenTransport searchTransport (String id) {
    for (int i = 0; i < gestionMoyenTransport.getTransports().size(); i++) {
      if (id.equalsIgnoreCase(gestionMoyenTransport.getTransports().get(i).getId()) ) {
        return gestionMoyenTransport.getTransports().get(i);
      }
    }
    return null;
  }

  public GestionLocation getGestionLocation() {
    return gestionLocation;
  }

  public GestionCompagnie getGestionCompagnie() {
    return gestionCompagnie;
  }

  public GestionTrajet getGestionTrajet() {
    return gestionTrajet;
  }

  public GestionMoyenTransport getGestionMoyenTransport() {
    return gestionMoyenTransport;
  }

  public void creerMoyenTransport (String id, ArrayList<Section> sections) {
    gestionMoyenTransport.creer(fabriqueAerienne, id, sections);
  }


  public Section creerSection (String id, int nbSeats, String disposition, int prixSection) {
    return fabriqueAerienne.creerSection(id, nbSeats, disposition, prixSection);
  }

  public boolean modifierVille (String id, String ville) {
    for (int i = 0; i < gestionLocation.getLocations().size(); i++) {
      if ( id.equalsIgnoreCase(gestionLocation.getLocations().get(i).getId()) ) {
        gestionLocation.getLocations().get(i).setVille(ville);
        return true;
      }
    }
    return false;
  }







  public void creerDestination (String id, String villeDepart, String villeDestination, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    gestionLocation = new GestionLocation();
    gestionLocation.creer(fabriqueAerienne, id, villeDepart);
    gestionLocation.creer(fabriqueAerienne, id, villeDestination);
  }

  public Trajet getVol (GestionTrajet gestion, int id) {
    return gestion.getTrajets().get(id);
  }

  @Override
  public void creerDestination() {
    gestionLocation.creer();
  }

  @Override
  public void modifierDestination(int id) {
    // TODO Auto-generated method stub

  }

  @Override
  public void supprimerDestination(int id) {
    // TODO Auto-generated method stub

  }

  @Override
  public void affiche() {
    // TODO Auto-generated method stub
    for (int i = 0; i < gestionLocation.getLocations().size(); i++) {
      System.out.println(gestionLocation.getLocations().get(i));
    }

    for (int i = 0; i < gestionCompagnie.getCompagnies().size(); i++) {
      System.out.println(gestionCompagnie.getCompagnies().get(i));
    }

    for (int i = 0; i < gestionTrajet.getTrajets().size() ; i++) {
      System.out.println(gestionTrajet.getTrajets().get(i));
    }
  }

  @Override
  public boolean verifierId() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void chercherVille() {
    // TODO Auto-generated method stub

  }


}

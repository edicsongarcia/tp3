package gestion;

import Location.*;
import compagnie.*;
import transport.MoyenTransport;
import transport.Section;
import voyage.*;

import java.util.ArrayList;

public abstract class AbstractFabrique {

  /**
   *
   * @param id
   * @param vil
   */
  public abstract Location creerLocation(String id,String vil);
  public abstract Trajet creerTrajet(String company, String id, String depart, String arrival, String dateDepart,
                                     String dateArrival, String heureDepart, String heureArrival);
  public abstract Compagnie creerCompagnie(String id, int prixSection);
  public abstract MoyenTransport creerMoyenDeTransport(String id, ArrayList<Section> sections);
  public abstract MoyenTransport creerMoyenDeTransport(String id, int rangees);
  public abstract Section creerSection(String id, int nbSeats, String disposition, int prixSection);
}
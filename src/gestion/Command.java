package gestion;

public interface Command {

  void execute();

}
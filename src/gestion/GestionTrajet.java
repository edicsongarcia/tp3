package gestion;

import voyage.*;

import java.util.ArrayList;

public class GestionTrajet implements Gestion {

  private ArrayList <Trajet> trajets;

  public GestionTrajet () {
    trajets = new ArrayList<>();
  }

  public void creerVol(AbstractFabrique fabrique, String company, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrival, String heureDepart, String heureArrival) {
    trajets.add(fabrique.creerTrajet(company, id, villeDepart, villeArrive, dateDepart, dateArrival, heureDepart, heureArrival));
  }

  public void creer() {

  }

  public void modifier(int id) {

  }

  public void supprimer(int id) {

  }

  public ArrayList<Trajet> getTrajets() {
    return trajets;
  }

  public void setTrajets(ArrayList<Trajet> tra) {
    this.trajets = tra;
  }


}
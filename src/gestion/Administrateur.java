package gestion;

public abstract class Administrateur {

  public abstract void creerDestination();

  public abstract boolean searchLocation (String location);

  public abstract boolean searchCompagnie (String compagnie);

  public abstract String getIdCompagnie (String company);

  public abstract void creerTrajet(String company, String locationDepart, String locationArrive,
                          String  dateDepart, String dateArrive, String heureDepart, String heureArrive );

  public abstract void modifierDestination(int id);

  public abstract void supprimerDestination(int id);

  public abstract void affiche();

  public abstract boolean verifierId();

  public abstract void chercherVille();
}

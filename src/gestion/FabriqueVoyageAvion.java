package gestion;

import transport.*;
import voyage.*;
import Location.*;
import compagnie.Compagnie;
import compagnie.*;

import java.util.ArrayList;

public class FabriqueVoyageAvion extends AbstractFabrique {

  /**
   *
   * @param id
   * @param vil
   */
  public Location creerLocation(String id,String vil) {
    return new Aeroport(id,vil);
  }

  @Override
  public Trajet creerTrajet(String company, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrival, String heureDepart, String heureArrival) {
    return new Vol(company, id, villeDepart, villeArrive, dateDepart, dateArrival, heureDepart, heureArrival);
  }

  public Compagnie creerCompagnie(String id, int prixSection) {
    return new Aerienne(id, prixSection);
  }

  public MoyenTransport creerMoyenDeTransport(String id, int ranges) {
    return new Avion(id, ranges);
  }

  public MoyenTransport creerMoyenDeTransport (String id, ArrayList<Section> sections) {
    return new Avion(id, sections);
  }

  @Override
  public Section creerSection(String id,  int nbSeats, String disposition, int prixSection) {
    Double price;
    int priceParSection;
    if (id.equalsIgnoreCase("1")) {
      return new Premier("F", nbSeats, disposition, prixSection);
    } else if (id.equalsIgnoreCase("2")) {
      price = prixSection * 0.75;
      priceParSection = price.intValue();
      return new Affaire("A", nbSeats, disposition, priceParSection);
    } else if (id.equalsIgnoreCase("3")) {
      price = prixSection * 0.6;
      priceParSection = price.intValue();
      return new EconomiquePremium("P", nbSeats, disposition, priceParSection);
    } else {
      price = prixSection * 0.5;
      priceParSection = price.intValue();
      return new Economique("E", nbSeats, disposition, priceParSection);
    }
  }

}
package voyage;

public class Croisier extends Trajet {

  public Croisier (String compagnie, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    super(compagnie, id, villeDepart, villeArrive, dateDepart, dateArrive, heureDepart, heureArrive);
  }

  public Croisier (String id) {
    super(id);
  }

}
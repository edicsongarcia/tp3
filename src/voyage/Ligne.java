package voyage;

public class Ligne extends Trajet {

  public Ligne (String company, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    super(company, id, villeDepart, villeArrive, dateDepart, dateArrive, heureDepart, heureArrive);
  }

  public Ligne(String id) {
    super(id);
  }
}
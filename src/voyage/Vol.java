package voyage;

public class Vol extends Trajet {

  public Vol (String compagnie, String id, String villeDepart, String villeDestination, String departDate, String arriveDate, String heureDepart, String heureArrive) {
    super(compagnie, id, villeDepart, villeDestination, departDate, arriveDate, heureDepart, heureArrive);
  }

  public Vol (String id) {
    super(id);
  }
}
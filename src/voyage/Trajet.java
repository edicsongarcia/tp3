package voyage;

public class Trajet {
  private String id;
  private String compagnie;
  private String dateDepart;
  private String dateArrive;
  private String heureDepart;
  private String heureArrive;
  private String villeDepart;
  private String villeArrive;

  public Trajet () {}

  public Trajet(String compagnie, String id, String villeDepart, String villeArrive, String dateDepart, String dateArrive, String heureDepart, String heureArrive) {
    this.id = id;
    this.compagnie = compagnie;
    this.dateDepart = dateDepart;
    this.dateArrive = dateArrive;
    this.heureDepart = heureDepart;
    this.heureArrive = heureArrive;
    this.villeArrive = villeArrive;
    this.villeDepart = villeDepart;
  }

  public String getCompagnie() {
    return compagnie;
  }


  public void setCompagnie(String compagnie) {
    this.compagnie = compagnie;
  }

  public String getVilleDepart() {
    return villeDepart;
  }

  public String getVilleArrive() {
    return villeArrive;
  }

  public void setVilleDepart(String villeDepart) {
    this.villeDepart = villeDepart;
  }

  public void setVilleArrive(String villeArrive) {
    this.villeArrive = villeArrive;
  }

  public Trajet (String id) {
    this.id = id;
  }


  public String getId() {
    return id;
  }

  public String getDateDepart() {
    return dateDepart;
  }

  public String getDateArrive() {
    return dateArrive;
  }

  public String getHeureDepart() {
    return heureDepart;
  }

  public String getHeureArrive() {
    return heureArrive;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setDateDepart(String dateDepart) {
    this.dateDepart = dateDepart;
  }

  public void setDateArrive(String dateArrive) {
    this.dateArrive = dateArrive;
  }

  public void setHeureDepart(String heureDepart) {
    this.heureDepart = heureDepart;
  }

  public void setHeureArrive(String heureArrive) {
    this.heureArrive = heureArrive;
  }
}
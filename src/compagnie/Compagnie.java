package compagnie;

public abstract class Compagnie {

  private String id;
  private int prixSection;

  public Compagnie(String id, int prixSection) {
    this.id = id;
    this.prixSection = prixSection;
  }

  public String toString() {
    return "id= " + id + " prix =" + prixSection;
  }

  public String getId() {
    return id;
  }

  public int getPrixSection() {
    return prixSection;
  }
}

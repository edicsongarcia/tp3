package GUI;

import reservation.Passager;
import reservation.Reservation;
import reservation.ReservationVol;


import java.util.ArrayList;
import java.util.Scanner;

public class ClientUI {

  ArrayList<Reservation> passager_storage = new ArrayList<>();

  boolean first_exit = false;
  boolean second_exit = false;
  private Subject subject;
  private Client client;
  //voyage.Vol volaffiche = (Vol) AdminUI.storage.get(0);


  public ClientUI(Subject subject, Client client) {
    this.subject = subject;
    this.client = client;
  }

  public void affichageMenu() {


    while (!first_exit) {


      System.out.println("------------------------------------------------------------------");
      System.out.println("please choose one of next options :");
      System.out.println("------------------------------------------------------------------");

      System.out.println("1:Recherche de trajet \n" +
              "2:Reservation \n" +
              "3:Payment \n" +
              "4:Change or Cancel \n" +
              "5:Return to principal menu  \n" +
              "6:Exit \n");
      System.out.println("------------------------------------------------------------------");
      Scanner choice = new Scanner(System.in);
      int firstChoice = choice.nextInt();

      if (firstChoice == 1) {


        while (!second_exit) {
          System.out.println("------------------------------------------------------------------");

          System.out.println("Please write departure city:");
          String departure_city = choice.next();
          System.out.println("Please  write destination destination:");
          String destination_city = choice.next();
          System.out.println("Please  write date of departure");
          String date_departure = choice.next();
          System.out.println("------------------------------------------------------------------");

          for (int i = 0; i < client.getLocations().size(); i++) {
            if (client.getTrajets().get(i).getVilleDepart().equalsIgnoreCase(departure_city) && client.getTrajets().get(i)
                    .getVilleArrive().equalsIgnoreCase(destination_city) && client.getTrajets().get(i).getDateDepart().
                    equalsIgnoreCase(date_departure)) {

              //Available choices works for clients

              System.out.println("------------------------------------------------------------------");
              System.out.println("AVAILABLE CHOICE:");
              System.out.println("------------------------------------------------------------------");
              System.out.println(client.getTrajets().get(i).getId() + "|" + client.getTrajets().get(i).getVilleDepart() +
                      "|" + client.getTrajets().get(i).getVilleArrive() + "|" + client.getTrajets().get(i).getDateDepart()
                      + "|" + client.getTrajets().get(i).getDateArrive() + "|" + client.getTrajets().get(i).getHeureDepart()
                      + "|" + client.getTrajets().get(i).getHeureArrive() + "|" + client.getTrajets().get(i).getCompagnie()
              );
              System.out.println("------------------------------------------------------------------");


            }
          }
          second_exit = true;

        }

        //Reservation

      } else if (firstChoice == 2) {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Please provide your Information:");
        System.out.println("------------------------------------------------------------------");

        System.out.println("ID Transport:");
        String IdTransport = choice.next();
        for (int i = 0; i < client.getLocations().size(); i++) {

          if (IdTransport.equalsIgnoreCase(client.getTrajets().get(i).getId())) {
            String Credit_cart = null;
            String Date_expire = null;
            String reservation = null;

            System.out.println("Passenger's name:");
            String Name = choice.next();
            System.out.println("Passenger's family ");
            String family_name = choice.next();
            System.out.println("Passport Numbers:");
            String passport = choice.next();

//

            System.out.println("You want to pay OR reserve?");
            String ReserveOrPay = choice.next();
            if (ReserveOrPay.equalsIgnoreCase("Reserve")) {
              Credit_cart = "NOT PAID";
              Date_expire = "NOT PAID";
              reservation = "Reserve";


            } else if (ReserveOrPay.equalsIgnoreCase("pay")) {
              System.out.println("Credit Cart Number ");
              Credit_cart = choice.next();
              System.out.println("Date Expire");
              Date_expire = choice.next();
              reservation = "Purchased";

            }
            System.out.println("passenger class:");
            String class_passenger = choice.next();

            System.out.println("seat Number");
            String seat_number = choice.next();

            Reservation passager = new ReservationVol(Name, family_name, passport, IdTransport, Credit_cart, Date_expire, reservation, class_passenger, seat_number);
            passager_storage.add(passager);

            System.out.println("------------------------------------------------------------------");

            System.out.println("..............Registration------> Success.........................");

            System.out.println("------------------------------------------------------------------");

            break;
          } else {

            System.out.println("------------------------------------------------------------------");

            System.out.println("--------------------- NOT FIND TRY AGAIN--------------------------");

            System.out.println("------------------------------------------------------------------");
            break;
          }


        }


      } else if (firstChoice == 3) {

        System.out.println("please provide Passenger information");

        System.out.println("Passenger's name:");
        String Name = choice.next();
        System.out.println("Passenger's family ");
        String family_name = choice.next();
        System.out.println("Passport Numbers:");
        String passport = choice.next();


        for (int i = 0; i < passager_storage.size(); i++) {
          if (passager_storage.get(i).getName().equalsIgnoreCase(Name) &&
                  passager_storage.get(i).getFamily().equalsIgnoreCase(family_name)
                  && passager_storage.get(i).getPassport_number().equalsIgnoreCase(passport)) {
            System.out.println("Credit Cart Number ");
            String Credit_cart = choice.next();
            System.out.println("Date Expire");
            String Date_expire = choice.next();
            String reservation = "Purchased";

            passager_storage.get(i).setCredit_cart(Credit_cart);
            passager_storage.get(i).setDate_expire(Date_expire);
            passager_storage.get(i).setReservation(reservation);

            System.out.println("------------------------------------------------------------------");

            System.out.println("----------------Payment--->   Success-----------------------------");


            System.out.println("------------------------------------------------------------------");


          } else {

            System.out.println("------------------------------------------------------------------");

            System.out.println("------------No reserved person by this information----------------");

            System.out.println("------------------------------------------------------------------");

          }

        }


      } else if (firstChoice == 4) {

        System.out.println("Change or Cancel?");

        String change_Cancel = choice.next();
        if (change_Cancel.equalsIgnoreCase("change")) {


          System.out.println("------------------------------------------------------------------");

          System.out.println("please provide Passenger information");

          System.out.println("------------------------------------------------------------------");

          System.out.println("Passenger's name:");
          String Name = choice.next();
          System.out.println("Passenger's family ");
          String family_name = choice.next();
          System.out.println("Passport Numbers:");
          String passport = choice.next();


          for (int i = 0; i < passager_storage.size(); i++) {
            if (passager_storage.get(i).getName().equalsIgnoreCase(Name) &&
                    passager_storage.get(i).getFamily().equalsIgnoreCase(family_name)
                    && passager_storage.get(i).getPassport_number().equalsIgnoreCase(passport)) {
              System.out.println(" New ID Trasnport ");
              String ID_transport = choice.next();

              System.out.println("------------------------------------------------------------------");

              System.out.println("------------------CHANGE----> Success-----------------------------");


              System.out.println("------------------------------------------------------------------");

              for (int j = 0; j < client.getLocations().size(); j++) {

                if (ID_transport.equalsIgnoreCase(client.getTrajets().get(j).getId())) {
                  passager_storage.get(j).setIdTransport(ID_transport);


                } else {
                  System.out.println("Can not find such a ID");
                }


              }


            } else {
              System.out.println("Can not find such a person");
            }


          }
        }

        if (change_Cancel.equalsIgnoreCase("Cancel")) {

          System.out.println("------------------------------------------------------------------");


          System.out.println("--------------please provide Passenger information-----------------");

          System.out.println("------------------------------------------------------------------");


          System.out.println("Passenger's name:");
          String Name = choice.next();
          System.out.println("Passenger's family ");
          String family_name = choice.next();
          System.out.println("Passport Numbers:");
          String passport = choice.next();


          for (int i = 0; i < passager_storage.size(); i++) {
            if (passager_storage.get(i).getName().equalsIgnoreCase(Name) &&
                    passager_storage.get(i).getFamily().equalsIgnoreCase(family_name)
                    && passager_storage.get(i).getPassport_number().equalsIgnoreCase(passport)) {
              passager_storage.remove(i);

              System.out.println("------------------------------------------------------------------");

              System.out.println("---------------------CANCEL----> Success--------------------------");


              System.out.println("------------------------------------------------------------------");


            }

          }


        } else if (firstChoice == 5) {
          first_exit = false;
        } else if (firstChoice == 6) {
          break;
        }


      }

      else if (firstChoice==5){

      first_exit=false;
      }

      else if (firstChoice==6){
        break;
      }
      }



    }


  }






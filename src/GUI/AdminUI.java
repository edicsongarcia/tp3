package GUI;

import java.util.ArrayList;
import java.util.Scanner;

import gestion.AdminAerienne;
import gestion.AdminMaritime;
import gestion.AdminTerrestre;
import gestion.Administrateur;
import transport.Section;


//
public class AdminUI {


  String adminUserNameSaved = "admin";
  String adminPasswordSaved = "admin";
  int adminType = 0;
  private Subject subjectAerienne;
  private AdministrateurAerienne administrateurAerienne;
  private Subject subjectMaritime;
  private AdministrateurMaritime administrateurMaritime;
  private Subject subjectTerrestre;
  private AdministrateurTerrestre administrateurTerrestre;

  private AdminAerienne adminAerienne = new AdminAerienne();
  private AdminMaritime adminMaritime = new AdminMaritime();
  private AdminTerrestre adminTerrestre = new AdminTerrestre();

  public AdminUI(Subject subject, AdministrateurAerienne administrateurAerienne) {
    this.subjectAerienne = subject;
    this.administrateurAerienne = administrateurAerienne;
    this.adminType = 1;
  }
  public AdminUI (Subject subject, AdministrateurMaritime administrateurMaritime) {
    this.subjectMaritime = subject;
    this.administrateurMaritime = administrateurMaritime;
    this.adminType = 2;
  }
  public AdminUI (Subject subject, AdministrateurTerrestre administrateurTerrestre) {
    this.subjectTerrestre = subject;
    this.administrateurTerrestre = administrateurTerrestre;
    this.adminType = 3;
  }

  //public AdminUI ();

  public boolean verifierLocations (Administrateur administrateur, String locationDepart, String locationArrive, String company) {
    if (!administrateur.searchLocation(locationDepart)) {
      System.out.println("------------------------------------------------------------------");

      System.out.println("\nThe departing location has not been yet included.\n" +
              "Create the location first\n.");
      System.out.println("------------------------------------------------------------------");

    } else if (!administrateur.searchLocation(locationArrive)) {

      System.out.println("------------------------------------------------------------------");

      System.out.println("\nThe arrival location has not been yet included.\n" +
              "Create the location first\n.");
      System.out.println("------------------------------------------------------------------");

    } else {
      if (administrateur.searchCompagnie(company) ){
        company = administrateur.getIdCompagnie(company);
        if (company != null) {
          return true;
        }
      } else {
        System.out.println("\nThere is no " + company + " company registered\n" +
                "Try Again.\n");
      }
    }
    return false;
  }


  public void afficher (Administrateur administrateur) {
    administrateur.affiche();
  }

  public void affichageMenu() {

    boolean exitMenu = false;
    String userName = "", password = "";

    while (!exitMenu) {

      if (adminType > 0 && adminType < 4) {
        System.out.println("------------------------------------------------------------------");

        System.out.println("------------------Please Enter the admin username-----------------:");
        System.out.println("------------------------------------------------------------------");

        Scanner adminUser = new Scanner(System.in);
        userName= adminUser.nextLine();
        System.out.println("------------------------------------------------------------------");

        System.out.println("Please enter the password:");
        System.out.println("------------------------------------------------------------------");

        password = adminUser.nextLine();

        if (adminUserNameSaved.equals(userName) & adminPasswordSaved.equals(password)) {
          Scanner choice = new Scanner(System.in);
          while(!exitMenu) {
            System.out.println("------------------------------------------------------------------");

            System.out.println("-----------------Welcome Administrator---------------------------");
            System.out.println("------------------------------------------------------------------");

            System.out.println("Please choose one of the following options: \n");
            System.out.println("------------------------------------------------------------------");

            System.out.println(
                              "1. Create.\n"
                            + "2. Modify\n"
                            + "3. Erase\n"
                            + "4. Return to the previous menu\n"
                            + "5. Exit\n");

            System.out.println("------------------------------------------------------------------");

            int choix = choice.nextInt();

            if (choix == 1) {
              // Create

              while (!exitMenu) {
                System.out.println("------------------------------------------------------------------");

                System.out.println("Please choose the type to create:");
                System.out.println("------------------------------------------------------------------");

                System.out.println(
                                "1. Location\n" +
                                "2. Compagnie\n" +
                                "3. Trajet\n" +
                                "4. Section\n" +
                                "5. Return to previous menu\n" +
                                "6. Exit\n");
                System.out.println("------------------------------------------------------------------");


                choix = choice.nextInt();

                if (choix == 1) {
                  // Creation des locations
                  System.out.println("------------------------------------------------------------------");

                  System.out.println("Enter the name of the city :");
                  System.out.println("------------------------------------------------------------------");

                  String ville = choice.next();

                  String id = ville.substring(0,2);

                  if (adminType == 1) {
                    adminAerienne.creerLocation(id, ville);
                    subjectAerienne.setLocations(adminAerienne.getGestionLocation().getLocations());
                  } else if (adminType == 2) {
                    adminMaritime.creerLocation(id, ville);
                    subjectMaritime.setLocations(adminMaritime.getGestionLocation().getLocations());
                  } else {
                    adminTerrestre.creerLocation(id, ville);
                    subjectTerrestre.setLocations(adminTerrestre.getGestionLocation().getLocations());

                  }

                  System.out.println("------------------------------------------------------------------");

                  System.out.println("------------------Location created successfully-------------------");

                  System.out.println("------------------------------------------------------------------");



                } else if (choix == 2) {
                  // Creation des compagnies
                  boolean name = false;
                  int prixSection = 0;
                  String company = "";
                  while (!name) {
                    System.out.println("------------------------------------------------------------------");

                    System.out.println("Enter the name of the company (6 characters minimum) :");
                    System.out.println("------------------------------------------------------------------");

                    company = choice.next();
                    if (company.length() < 6) {
                      System.out.println("------------------------------------------------------------------");

                      System.out.println("You need more characters... \nTry again");
                      System.out.println("------------------------------------------------------------------");

                    } else {
                      System.out.println("------------------------------------------------------------------");

                      System.out.println("Enter the price for the most expensive section");
                      System.out.println("------------------------------------------------------------------");

                      prixSection = choice.nextInt();
                      name = true;
                    }
                  }
                  if (adminType == 1) {
                    adminAerienne.creerCompagnie(company, prixSection);
                    subjectAerienne.setCompagnies(adminAerienne.getGestionCompagnie().getCompagnies());
                  } else if (adminType == 2) {
                    adminMaritime.creerCompagnie(company, prixSection);
                    subjectMaritime.setCompagnies(adminMaritime.getGestionCompagnie().getCompagnies());
                  } else {
                    adminTerrestre.creerCompagnie(company, prixSection);
                    subjectTerrestre.setCompagnies(adminTerrestre.getGestionCompagnie().getCompagnies());
                  }


                  System.out.println("------------------------------------------------------------------");

                  System.out.println("\nThe company has been created\n");
                  System.out.println("------------------------------------------------------------------");


                } else if (choix == 3) {
                  // Creation des trajets

                  String locationDepart ="", locationArrive = "", dateDepart = "", dateArrive = "";
                  String heureDepart = "", heureArrive = "";

                  System.out.println("Please, enter the details for the trip: \n" +
                          "Enter the departure location");
                  locationDepart = choice.next();
                  System.out.println("Enter the arrival location");
                  locationArrive = choice.next();
                  if (adminType == 3) {
                    boolean garesAdditionel = true;
                    while (garesAdditionel) {
                      System.out.println("Do you want to add additional stops?\n" +
                              "1. Yes\n" +
                              "2. No\n");
                      choix = choice.nextInt();
                      if (choix != 1) {
                        garesAdditionel = false;
                        break;
                      }
                      System.out.println("Enter the name of the stop:");
                      String stop = choice.next();

                      String idStop = "IDStop";
                      if (!adminTerrestre.searchLocation(stop)) {
                        // creer et ajouter la location a la liste
                        adminTerrestre.creerStop(idStop, stop);
                      } else {
                        // trouver la location et l'ajouter a la liste d'arretes
                      }
                    }
                  }
                  System.out.println("Enter the date of departure:");
                  dateDepart = choice.next();
                  System.out.println("Enter the departure time");
                  heureDepart = choice.next();
                  System.out.println("Enter the date of arrival");
                  dateArrive = choice.next();
                  if ("".equals(dateArrive)) {
                    dateArrive = dateDepart;
                  }
                  System.out.println("Enter the time of arrival (estimated)");
                  heureArrive = choice.next();

                  String company = "";
                  System.out.println("Enter the name of the company: ");
                  company = choice.next();

                  if (adminType == 1) {

                    if (verifierLocations (adminAerienne, locationDepart, locationArrive, company) ) {
                      adminAerienne.creerTrajet(company, locationDepart, locationArrive, dateDepart, dateArrive, heureDepart, heureArrive);
                      administrateurAerienne.setTrajets(adminAerienne.getGestionTrajet().getTrajets());
                      System.out.println("\nThe flight has been created!\n");
                    }

                  } else if (adminType == 2) {

                    if (dateDepart.equalsIgnoreCase(dateArrive)) {
                      System.out.println("La departure date has to be diferent of the arrival date.\n" +
                              "Try again\n");
                    } else if ( ! locationDepart.equalsIgnoreCase(locationArrive)) {
                      System.out.println("The departure location has to be the same as the arrival location.\n" +
                              "Try Again\n");
                    } else {
                      if (verifierLocations(adminMaritime, locationDepart, locationArrive, company)) {
                        adminMaritime.creerTrajet(company, locationDepart, locationArrive, dateDepart, dateArrive, heureDepart, heureArrive);
                        administrateurMaritime.setTrajets(adminMaritime.getGestionTrajet().getTrajets());
                        System.out.println("\nThe liner has been created\n");
                      }
                    }

                  } else {
                    if (verifierLocations(adminTerrestre, locationDepart, locationArrive, company)) {
                      adminTerrestre.creerTrajet(company, locationDepart, locationArrive, dateDepart, dateArrive, heureDepart, heureArrive);
                      administrateurTerrestre.setTrajets(adminTerrestre.getGestionTrajet().getTrajets());
                      System.out.println("\nThe journey has been created.\n");
                    }
                  }
                } else if (choix == 4) {
                  // Creation des sections

                  if (adminType == 1) {

                    // create a plane
                    int sectionId = 0;
                    int disposition = 0;
                    int nb_seats = 0;
                    String companyName = "";
                    String dispo = "";
                    int prixSection = 0;
                    ArrayList<Integer> sectionCree = new ArrayList<>();
                    ArrayList<Section> sectionList = new ArrayList<>();

                    while(true) {
                      System.out.println("Enter the name of the company:");
                      companyName = choice.next();
                      if (companyName.length() < 6) {
                        System.out.println("You need more 6 characters. Try again");
                        continue;
                      }if (adminAerienne.searchCompagnie(companyName)) {
                        prixSection = adminAerienne.getPriceSectionCompagnieById(companyName);
                        break;
                      } else {
                        System.out.println("The company does not exists. Try again\n");
                        continue;
                      }
                    }

                    while (!exitMenu) {
                      System.out.println("Please choose the type of section\n" +
                              "1. Premier\n" +
                              "2. Affaire\n" +
                              "3. Economique Premium\n" +
                              "4. Economique\n" +
                              "5. Return to the previous menu\n");
                      sectionId = choice.nextInt();
                      if (sectionId == 5) {
                        break;
                      }
                      if (!sectionCree.contains(sectionId)) {
                        sectionCree.add(sectionId);
                        while (true) {
                          System.out.println("What is the disposition of the seats inside this section\n" +
                                  "1. Etroit\n" +
                                  "2. Confort\n" +
                                  "3. Moyen\n" +
                                  "4. Large\n");
                          disposition = choice.nextInt();
                          if (disposition > 0 && disposition < 5) {
                            if (disposition == 1) {
                              dispo = "S";
                            } else if (disposition == 2) {
                              dispo = "C";
                            } else if (disposition == 3) {
                              dispo = "M";
                            } else {
                              dispo = "L";
                            }
                            System.out.println("Enter the number of seats");
                            nb_seats = choice.nextInt();
                            break;
                          } else {
                            System.out.println("Invalid choice. try again.\n");
                          }
                        }



                        sectionList.add(adminAerienne.creerSection(sectionId + "", nb_seats, dispo, prixSection));
                        System.out.println("Section added\n");

                      } else {
                        System.out.println("The section already exists. Try again\n");
                        continue;
                      }
                      System.out.println("Do you want to add more sections?\n" +
                              "1. Yes\n" +
                              "2. No");
                      sectionId = choice.nextInt();
                      if (sectionId == 2) {
                        // create a Plane with the section and seats
                        System.out.println("Enter the wanted id");
                        String idPlane = choice.next();
                        adminAerienne.creerMoyenTransport(idPlane, sectionList);
                        administrateurAerienne.setMoyenTransports(adminAerienne.getGestionMoyenTransport().getTransports());
                        System.out.println("\nPlane created.\n");
                        System.out.println("\nReturning to the previous menu\n");
                        break;
                      }
                    }
                  } else if (adminType == 2) {

                    // Create a liner
                    int sectionId = 0;
                    int nbCabines = 0;
                    String companyName = "";
                    String dispo = "";
                    int prixSection = 0;
                    ArrayList<Integer> sectionCree = new ArrayList<>();
                    ArrayList<Section> sectionList = new ArrayList<>();

                    while(true) {
                      System.out.println("Enter the name of the company:");
                      companyName = choice.next();
                      if (companyName.length() < 6) {
                        System.out.println("You need more 6 characters. Try again");
                        continue;
                      }if (adminMaritime.searchCompagnie(companyName)) {
                        prixSection = adminMaritime.getPriceSectionCompagnieById(companyName);
                        break;
                      } else {
                        System.out.println("The company does not exists. Try again\n");
                        continue;
                      }
                    }
                    while (!exitMenu) {
                      System.out.println("Please choose the type of section\n" +
                              "1. Interieur\n" +
                              "2. Ocean\n" +
                              "3. Suite\n" +
                              "4. Famille\n" +
                              "5. Famille Deluxe\n" +
                              "6. Return to the previous menu\n");
                      sectionId = choice.nextInt();
                      if (sectionId == 6) {
                        break;
                      }
                      if (!sectionCree.contains(sectionId)) {
                        sectionCree.add(sectionId);
                        System.out.println("Enter the number of cabins");
                        nbCabines = choice.nextInt();
                        sectionList.add(adminMaritime.creerSection(sectionId + "", nbCabines, dispo, prixSection));
                        System.out.println("Section added\n");
                      } else {
                        System.out.println("The section already exists. Try again\n");
                        continue;
                      }
                      System.out.println("Do you want to add more sections?\n" +
                              "1. Yes\n" +
                              "2. No");
                      sectionId = choice.nextInt();
                      if (sectionId == 2) {
                        // create a liner with the section and seats
                        System.out.println("Enter the wanted id");
                        String idLiner = choice.next();
                        adminMaritime.creerMoyenTransport(idLiner, sectionList);
                        administrateurMaritime.setMoyenTransports(adminMaritime.getGestionMoyenTransport().getTransports());
                        System.out.println("\nLiner created.\n");
                        System.out.println("\nReturning to the previous menu\n");
                        break;
                      }
                    }
                  } else {
                    // Create train

                    int sectionId = 0;
                    int nbSeats = 0;
                    String companyName = "";
                    int prixSection = 0;
                    ArrayList<Integer> sectionCree = new ArrayList<>();
                    ArrayList<Section> sectionList = new ArrayList<>();

                    while(true) {
                      System.out.println("Enter the name of the company:");
                      companyName = choice.next();
                      if (companyName.length() < 6) {
                        System.out.println("You need more 6 characters. Try again");
                        continue;
                      }if (adminAerienne.searchCompagnie(companyName)) {
                        prixSection = adminAerienne.getPriceSectionCompagnieById(companyName);
                        break;
                      } else {
                        System.out.println("The company does not exists. Try again\n");
                        continue;
                      }
                    }
                    while (!exitMenu) {
                      System.out.println("Please choose the type of section\n" +
                              "1. Premier\n" +
                              "2. Economie\n" +
                              "3. Return to the previous menu\n");
                      sectionId = choice.nextInt();
                      if (sectionId == 3) {
                        break;
                      }
                      if (!sectionCree.contains(sectionId)) {
                        sectionCree.add(sectionId);
                        System.out.println("Enter the number of seats");
                        nbSeats = choice.nextInt();
                        sectionList.add(adminTerrestre.creerSection(sectionId + "", nbSeats, "S", prixSection));
                        System.out.println("Section added\n");
                      } else {
                        System.out.println("The section already exists. Try again\n");
                        continue;
                      }
                      System.out.println("Do you want to add more sections?\n" +
                              "1. Yes\n" +
                              "2. No");
                      sectionId = choice.nextInt();
                      if (sectionId == 2) {
                        // create a train with the section and seats
                        System.out.println("Enter the wanted id");
                        String idTrain = choice.next();
                        adminTerrestre.creerMoyenTransport(idTrain, sectionList);
                        administrateurTerrestre.setMoyenTransports(adminTerrestre.getGestionMoyenTransport().getTransports());
                        System.out.println("\nLiner created.\n");
                        System.out.println("\nReturning to the previous menu\n");
                        break;
                      }
                    }
                  }
                } else if (choix == 5) {
                  // return to previous menu
                  break;
                } else {
                  // Exit application
                  exitMenu = true;
                  break;
                }
              }

            } else if (choix == 2) {

              // Modify

              System.out.println("Please choose the type to modify:");
              System.out.println(
                              "1. Location\n" +
                              "2. Compagnie\n" +
                              "3. Trajet\n" +
                              "4. Section\n" +
                              "5. Return to previous menu\n" +
                              "6. Exit\n");

              choix = choice.nextInt();

              if (choix == 1) {
                // Location
                System.out.println("Enter the city id to modify");
                String idVille = choice.next();
                System.out.println("Enter the new name for the city");
                String nomVille = choice.next();
                if (adminType == 1) {
                  if (adminAerienne.modifierVille(idVille, nomVille) ){
                    System.out.println("The change has been made\n");
                  } else {
                    System.out.println("The id doesn't exists\n");
                  }
                } else if (adminType == 2) {
                  if (adminMaritime.modifierVille(idVille, nomVille) ){
                    System.out.println("The change has been made\n");
                  } else {
                    System.out.println("The id doesn't exists\n");
                  }
                } else {
                  if (adminTerrestre.modifierVille(idVille, nomVille) ){
                    System.out.println("The change has been made\n");
                  } else {
                    System.out.println("The id doesn't exists\n");
                  }
                }

              } else if (choix == 2) {
                // Company
                System.out.println("Enter the compagnie id to modify");
                String idCompagnie = choice.next();
                if (adminAerienne.searchCompagnie(idCompagnie)) {
                  System.out.println("Enter the parameter to modify for the company");

                  // TODO

                  String nomVille = choice.next();
                } else {
                  System.out.println("There is no such company");
                }


              } else if (choix == 3) {
                // Trajet

              } else if (choix == 4) {
                // Section

              } else if (choix == 5) {
                // return to previous menu
                break;
              } else {
                // exit
                exitMenu = true;
                break;
              }


            } else if (choix == 3) {

              // Erase
              System.out.println("Please choose the type to erase:");
              System.out.println(
                              "1. Location\n" +
                              "2. Compagnie\n" +
                              "3. Trajet\n" +
                              "4. Section\n" +
                              "5. Return to previous menu\n" +
                              "6. Exit\n");

              choix = choice.nextInt();

              if (choix == 1) {
                // Location

              } else if (choix == 2) {
                // Company

              } else if (choix == 3) {
                // Trajet

              } else if (choix == 4) {
                // Section

              } else if (choix == 5) {
                // return to previous menu
                break;
              } else {
                // exit
                exitMenu = true;
                break;
              }

            } else if (choix == 4){

              // Exit

              System.out.println("Exiting the menu...\n");
              break;

            } else if (choix == 5) {
              exitMenu = true;
              break;
            } else {
              System.out.println("Error! That's not an option");
            }

          }
        } else {
          System.out.println("\nWrong user name or password\n");
          break;
        }





      } else if (adminType == 4) {
        exitMenu = true;
        //break;
      } else {
        System.out.println("Error. Try again.\n");
      }

    }





  }
}


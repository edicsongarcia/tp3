package GUI;

public interface Observable {
    public void registrer(Observer observer);
    public void effacer (Observer observer);
    public void notifier();
}

package GUI;

import Location.Location;
import compagnie.Compagnie;
import transport.MoyenTransport;
import voyage.Trajet;

import java.util.ArrayList;

public class SubjectMaritime extends Subject {

    public SubjectMaritime (ArrayList<Location> locations, ArrayList<Compagnie> compagnies, ArrayList<Trajet> trajets,
                            ArrayList<MoyenTransport> moyenTransports, ArrayList<Observer> observers) {
        super(locations, compagnies, trajets, moyenTransports, observers);
    }
}

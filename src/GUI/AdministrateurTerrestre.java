package GUI;

import Location.Location;
import compagnie.Compagnie;
import transport.MoyenTransport;
import voyage.Trajet;

import java.util.ArrayList;

public class AdministrateurTerrestre extends Admin {

    public AdministrateurTerrestre (Subject subject, ArrayList<Location> locations, ArrayList<Compagnie> compagnies, ArrayList<Trajet> trajets,
                          ArrayList<MoyenTransport> moyenTransports) {
        super(subject, locations, compagnies, trajets, moyenTransports);
    }

}

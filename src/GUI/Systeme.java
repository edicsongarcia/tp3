package GUI;

import java.util.ArrayList;
import java.util.Scanner;

import Location.Location;
import compagnie.Compagnie;
import gestion.*;
import transport.MoyenTransport;
import voyage.Trajet;

public class Systeme {
  static String adminUserNameSaved ="admin";
  static String adminPasswordSaved ="admin";

  GestionLocation gestionLocation = null;

  public static void fillTabsAir (Subject subject, AdminAerienne administrateur) {
    subject.setLocations(administrateur.getGestionLocation().getLocations());
    subject.setCompagnies(administrateur.getGestionCompagnie().getCompagnies());
    subject.setTrajets(administrateur.getGestionTrajet().getTrajets());
    subject.setMoyenTransports(administrateur.getGestionMoyenTransport().getTransports());
  }

  public static void fillTabsSea (Subject subject, AdminMaritime administrateur) {
    subject.setLocations(administrateur.getGestionLocation().getLocations());
    subject.setCompagnies(administrateur.getGestionCompagnie().getCompagnies());
    subject.setTrajets(administrateur.getGestionTrajet().getTrajets());
    subject.setMoyenTransports(administrateur.getGestionMoyenTransport().getTransports());
  }

  public static void fillTabsLand (Subject subject, AdminTerrestre administrateur) {
    subject.setLocations(administrateur.getGestionLocation().getLocations());
    subject.setCompagnies(administrateur.getGestionCompagnie().getCompagnies());
    subject.setTrajets(administrateur.getGestionTrajet().getTrajets());
    subject.setMoyenTransports(administrateur.getGestionMoyenTransport().getTransports());
  }

  public static void main(String[] args) {

    // Creation des subjets a observer
    Subject subjectAerienne = new SubjectAerienne( new ArrayList<Location>(), new ArrayList< Compagnie >(), new ArrayList< Trajet >(),
            new ArrayList< MoyenTransport >(), new ArrayList<Observer>() );
    Subject subjectMaritime = new SubjectMaritime( new ArrayList<Location>(), new ArrayList< Compagnie >(), new ArrayList< Trajet >(),
            new ArrayList< MoyenTransport >(), new ArrayList<Observer>() );
    Subject subjectTerrestre = new SubjectTerrestre( new ArrayList<Location>(), new ArrayList< Compagnie >(), new ArrayList< Trajet >(),
            new ArrayList< MoyenTransport >(), new ArrayList<Observer>() );

    // Creation des observateurs
    Admin adminAerienne = new AdministrateurAerienne(subjectAerienne, subjectAerienne.getLocations(), subjectAerienne.getCompagnies(),
            subjectAerienne.getTrajets(), subjectAerienne.getMoyenTransports());
    Admin adminMaritime = new AdministrateurMaritime(subjectMaritime, subjectMaritime.getLocations(), subjectMaritime.getCompagnies(),
            subjectMaritime.getTrajets(), subjectMaritime.getMoyenTransports());
    Admin adminTerrestre = new AdministrateurTerrestre(subjectTerrestre, subjectTerrestre.getLocations(), subjectTerrestre.getCompagnies(),
            subjectTerrestre.getTrajets(), subjectTerrestre.getMoyenTransports());

    Client clientAerienne = new ClientAerienne(subjectAerienne, subjectAerienne.getLocations(), subjectAerienne.getCompagnies(),
            subjectAerienne.getTrajets(), subjectAerienne.getMoyenTransports());
    Client clientMaritime = new ClientMaritime(subjectMaritime, subjectMaritime.getLocations(), subjectMaritime.getCompagnies(),
            subjectMaritime.getTrajets(), subjectMaritime.getMoyenTransports());
    Client clientTerrestre = new ClientTerrestre(subjectTerrestre, subjectTerrestre.getLocations(), subjectTerrestre.getCompagnies(),
            subjectTerrestre.getTrajets(), subjectTerrestre.getMoyenTransports());

    // Ajoute a las liste des observateurs
    subjectAerienne.registrer(adminAerienne);
    subjectAerienne.registrer(clientAerienne);
    subjectMaritime.registrer(adminMaritime);
    subjectMaritime.registrer(clientMaritime);
    subjectTerrestre.registrer(adminTerrestre);
    subjectTerrestre.registrer(clientTerrestre);

    // Remplir des donnees pour tester
    AdminAerienne adminTest = new AdminAerienne();
    fillTabsAir(subjectAerienne, adminTest);
    AdminMaritime adminTest2 = new AdminMaritime();
    fillTabsSea(subjectMaritime, adminTest2);
    AdminTerrestre adminTest3 = new AdminTerrestre();
    fillTabsLand(subjectTerrestre, adminTest3);


    boolean exit = false;
    while (!exit) {

      System.out.println("------------------------------------------------------------------");

      System.out.println("*************   Welcome to the Resevation system    ************* ");

      System.out.println("------------------------------------------------------------------");


      System.out.println("If you are a client press 1, if you are an admin press 2.");
      System.out.println("For exit press any other number.");

      System.out.println("------------------------------------------------------------------");


      Scanner choice = new Scanner(System.in);
      int firstChoice = choice.nextInt();

      if (firstChoice==2) {


        int adminType = 0;


        System.out.println("------------------------------------------------------------------");

        System.out.println("Enter the type of manager \n"
                + "1: Air\n"
                + "2: Maritime\n"
                + "3: Terrestrial\n"
                + "4: Exit\n");

        System.out.println("------------------------------------------------------------------");


        Scanner type = new Scanner(System.in);
        adminType = type.nextInt();
        if (adminType == 1) {
          AdminUI adminUI = new AdminUI( subjectAerienne, (AdministrateurAerienne) adminAerienne);
          adminUI.affichageMenu();
        } else if (adminType == 2) {
          AdminUI adminUI = new AdminUI( subjectMaritime, (AdministrateurMaritime) adminMaritime);
          adminUI.affichageMenu();
        } else if (adminType == 3) {
          AdminUI adminUI = new AdminUI( subjectTerrestre, (AdministrateurTerrestre) adminTerrestre);
          adminUI.affichageMenu();
        } else {
          System.out.println("Thank you. Bye");
          break;
        }


      } else if (firstChoice==1) {
        /////////////////////////////////// creation pour client

        // Sending the client with the information
        ClientUI clientUI = new ClientUI(subjectAerienne, clientAerienne);
        clientUI.affichageMenu();


        }


       else {
        exit = true;

        System.out.println("------------------------------------------------------------------");

        System.out.println("Thank you. Bye!");

        System.out.println("------------------------------------------------------------------");

      }

    }


  }


}
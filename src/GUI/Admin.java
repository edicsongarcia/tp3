package GUI;

import Location.Location;
import compagnie.Compagnie;
import transport.MoyenTransport;
import voyage.Trajet;

import java.util.ArrayList;

public class Admin implements Observer {

    private Subject subjet;
    private ArrayList<Location> locations;
    private ArrayList<Compagnie> compagnies;
    private ArrayList<Trajet> trajets;
    private ArrayList<MoyenTransport> moyenTransports;

    public Admin (Subject subject, ArrayList<Location> locations, ArrayList<Compagnie> compagnies, ArrayList<Trajet> trajets,
                   ArrayList<MoyenTransport> moyenTransports) {
        this.subjet = subject;
        this.locations = locations;
        this.compagnies = compagnies;
        this.trajets = trajets;
        this.moyenTransports = moyenTransports;
    }

    @Override
    public void update() {
        this.locations = this.subjet.getLocations();
        this.compagnies = this.subjet.getCompagnies();
        this.trajets = this.subjet.getTrajets();
        this.moyenTransports = this.subjet.getMoyenTransports();
    }

    public Subject getSubjet() {
        return subjet;
    }

    public void setSubjet(Subject subjet) {
        this.subjet = subjet;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    public ArrayList<Compagnie> getCompagnies() {
        return compagnies;
    }

    public void setCompagnies(ArrayList<Compagnie> compagnies) {
        this.compagnies = compagnies;
    }

    public ArrayList<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(ArrayList<Trajet> trajets) {
        this.trajets = trajets;
    }

    public ArrayList<MoyenTransport> getMoyenTransports() {
        return moyenTransports;
    }

    public void setMoyenTransports(ArrayList<MoyenTransport> moyenTransports) {
        this.moyenTransports = moyenTransports;
    }
}

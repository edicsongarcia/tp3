package GUI;

import Location.Location;
import compagnie.Compagnie;
import transport.MoyenTransport;
import voyage.Trajet;

import java.util.ArrayList;

public class Subject implements Observable {
    private ArrayList<Location> locations;
    private ArrayList<Compagnie> compagnies;
    private ArrayList<Trajet> trajets;
    private ArrayList<MoyenTransport> moyenTransports;
    private ArrayList<Observer> observers;
    private boolean change;


    public Subject () {
        locations = new ArrayList<>();
        compagnies = new ArrayList<>();
        trajets = new ArrayList<>();
        moyenTransports = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public Subject (ArrayList<Location> locations, ArrayList<Compagnie> compagnies, ArrayList<Trajet> trajets,
                    ArrayList<MoyenTransport> moyenTransports, ArrayList<Observer> observers) {

        this.locations = locations;
        this.compagnies = compagnies;
        this.trajets = trajets;
        this.moyenTransports = moyenTransports;
        this.observers = observers;
    }

    @Override
    public void registrer(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void effacer(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifier() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
        notifier();
    }

    public ArrayList<Compagnie> getCompagnies() {
        return compagnies;
    }

    public void setCompagnies(ArrayList<Compagnie> compagnies) {
        this.compagnies = compagnies;
        notifier();
    }

    public ArrayList<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(ArrayList<Trajet> trajets) {
        this.trajets = trajets;
        notifier();
    }

    public ArrayList<MoyenTransport> getMoyenTransports() {
        return moyenTransports;
    }

    public void setMoyenTransports(ArrayList<MoyenTransport> moyenTransports) {
        this.moyenTransports = moyenTransports;
        notifier();
    }

    public ArrayList<Observer> getObservers() {
        return observers;
    }

    public void setObservers(ArrayList<Observer> observers) {
        this.observers = observers;
        notifier();
    }

    public boolean isChange() {
        return change;
    }

    public void setChange(boolean change) {
        this.change = change;
    }
}

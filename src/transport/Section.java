package transport;

public abstract class Section {

  private String id;
  private int nbSeats;
  private String disposition;
  private int prixSection;

  public Section(String id, int nbSeats, int prixSection) {
    this.id = id;
    this.nbSeats = nbSeats;
    this.disposition = "";
    this.prixSection = prixSection;
  }

  public Section (String id, int nbSeats, String disposition, int prixSection) {
    this.id = id;
    this.nbSeats = nbSeats;
    this.disposition = disposition;
    this.prixSection = prixSection;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getNbSeats() {
    return nbSeats;
  }

  public void setNbSeats(int nbSeats) {
    this.nbSeats = nbSeats;
  }

  public String getDisposition() {
    return disposition;
  }

  public void setDisposition(String disposition) {
    this.disposition = disposition;
  }

  public int getPrixSection() {
    return prixSection;
  }

  public void setPrixSection(int prixSection) {
    this.prixSection = prixSection;
  }
}

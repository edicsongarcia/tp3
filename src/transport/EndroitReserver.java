package transport;

import reservation.Reservable;

public class EndroitReserver implements EtatEndroit {

  Reservable endroit;

  public EndroitReserver (Reservable endroit) {
    this.endroit = endroit;
  }

  @Override
  public boolean reserver() {
    System.out.println("Invalid operation.The place is already reserved.\n");
    return false;
  }

  @Override
  public boolean annuler() {
    this.endroit.changerEtat(new EndroitDisponible(this.endroit));
    return true;
  }

  @Override
  public boolean confirmer() {
    this.endroit.changerEtat(new EndroitConfimer(this.endroit));
    return true;
  }
}
package transport;

import java.util.ArrayList;

public class Bateau extends MoyenTransport {

  int nbCabines = 0;

  public Bateau (String id, int nbCabines) {
    this.nbCabines = nbCabines;
    this.setId(id);
    this.setSections(new ArrayList<>());
  }

  public Bateau (String id, ArrayList<Section> sections) {
    this.setId(id);
    this.setSections(sections);
  }

  @Override
  public void editSection(Section section) {

  }
}
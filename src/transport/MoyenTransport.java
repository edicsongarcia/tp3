package transport;

import java.util.ArrayList;

public abstract class MoyenTransport {

  private String id;
  private int colonnes;
  private ArrayList<Section> sections;

  public ArrayList<Section> getSections() {
    return sections;
  }

  public int getColonnes() {
    return colonnes;
  }

  public void setColonnes(int colonnes) {
    this.colonnes = colonnes;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setSections(ArrayList<Section> sections) {
    this.sections = sections;
  }

  public void addSection(Section section) {
    sections.add(section);
  }

  public void deleteSection (Section section) {
    sections.remove(section);
  }

  public Section getSection (String idSection) {
    for (int i = 0; i < sections.size(); i++) {
      if ( idSection.equals(sections.get(i)) ) {
        return sections.get(i);
      }
    }
    return null;
  }

  public abstract void editSection (Section section);

}
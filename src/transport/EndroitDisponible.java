package transport;

import reservation.Reservable;

public class EndroitDisponible implements EtatEndroit {

  Reservable endroit;

  public EndroitDisponible (Reservable endroit) {
    this.endroit = endroit;
  }

  @Override
  public boolean reserver() {
    this.endroit.changerEtat(new EndroitReserver(this.endroit));
    return true;
  }

  @Override
  public boolean annuler() {
    System.out.println("Invalid operation. you can't cancel a place available");
    return false;
  }

  @Override
  public boolean confirmer() {
    System.out.println("Invalid operation. you can't confirm a place available\n" +
            "You need to create a reservation before.");
    return false;
  }
}
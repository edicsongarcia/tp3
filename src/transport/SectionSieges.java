package transport;

public class SectionSieges extends Section {

  public SectionSieges (String id, int nbSieges, String disposition, int prixSection) {
    super(id, nbSieges, disposition, prixSection);
  }

  private String disposition;

  public String getDisposition() {
    return disposition;
  }

  public void setDisposition(String disposition) {
    this.disposition = disposition;
  }

  public int getPrixSection() {
    return this.getPrixSection();
  }

  public void setPrixSection(int prixSection) {
    this.setPrixSection(prixSection);
  }
}
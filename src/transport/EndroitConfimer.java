package transport;

import reservation.Reservable;

public class EndroitConfimer implements EtatEndroit {

  Reservable endroit;

  public EndroitConfimer (Reservable endroit) {
    this.endroit = endroit;
  }

  @Override
  public boolean reserver() {
    System.out.println("Invalid operation. You can't reserve a confirmed place.\n");
    return false;
  }

  @Override
  public boolean annuler() {
    this.endroit.changerEtat(new EndroitReserver(this.endroit));
    return true;
  }

  @Override
  public boolean confirmer() {
    System.out.println("Invalid operation.The place is already confirmed.\n");
    return false;
  }
}
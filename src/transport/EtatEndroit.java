package transport;

public interface EtatEndroit {
  public boolean reserver();
  public boolean annuler();
  public boolean confirmer();
}
package transport;

import java.util.ArrayList;

public class Avion extends MoyenTransport {

  private int ranges;
  private int colonnes;

  public Avion (String id, ArrayList<Section> sections) {
    this.setId(id);
    this.setSections(sections);
  }

  public Avion (String id, int ranges) {
    this.setSections(new ArrayList<>());
    this.setId(id);
    this.ranges = ranges;
  }

  public int getRanges() {
    return ranges;
  }

  public int getColonnes() {
    return colonnes;
  }

  public void setRanges(int ranges) {
    this.ranges = ranges;
  }

  public void setColonnes(int colonnes) {
    this.colonnes = colonnes;
  }

  @Override
  public void editSection(Section section) {

  }
}

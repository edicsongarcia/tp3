package transport;

import java.util.ArrayList;

public class Train extends MoyenTransport {

  private int ranges;

  public Train() {
    setSections(new ArrayList<>());
  }

  public Train (String id, int ranges) {
    setSections(new ArrayList<>());
    this.ranges = ranges;
    this.setId(id);
  }

  public Train (String id, ArrayList<Section> sections) {
    this.setId(id);
    this.setSections(sections);
  }

  public int getRanges() {
    return ranges;
  }

  public void setRanges(int ranges) {
    this.ranges = ranges;
  }

  @Override
  public void editSection(Section section) {

  }

}
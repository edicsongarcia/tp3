package Location;

public abstract class Location {

  private String id;
  private String ville;

  public Location(String id,String ville) {
    this.id=id;
    this.ville=ville;
  }


  public String toString() {
    return  "id=" + id + ", vil=" + ville ;
  }

  public String getVille() {
    return ville;
  }

  public String getId() {
    return id;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }
}